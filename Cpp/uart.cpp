#include <stdio.h> 
#include <stdlib.h>
#include <stddef.h> 
#include <termios.h> 
#include <fcntl.h> 
#include <unistd.h> 
#include <sys/types.h> 
 
int main() 
{ 
    int fd; 
    struct termios option; 
    char buf[20]; 
 
    system("echo ADAFRUIT-UART1 > /sys/devices/bone_capemgr.9/slots"); 
 
    fd = open("/dev/ttyO1", O_RDWR | O_NOCTTY); 
    if (fd < 0) printf("Error"); 
 
 
    option.c_cflag = B9600 | CS8 | CLOCAL | CREAD; 
    option.c_iflag = IGNPAR | IGNBRK; 
 
    option.c_oflag = 0; 
    option.c_lflag = 0; 
 
    option.c_cc[VTIME]=0; 
    option.c_cc[VMIN]=19; 
 
    tcflush(fd, TCIFLUSH); 
    tcsetattr(fd,TCSANOW,&option); 
 
    write(fd, "komunikacja" , 11); 
 
    read(fd, buf, 20); 
    printf("%s\n",buf); 
 
    close(fd); 
 
    return 0; 
}